/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.storage

import java.io._
import java.io.{IOException, File, FileOutputStream, RandomAccessFile}
import java.nio.ByteBuffer
import java.nio.channels.FileChannel.MapMode

import com.google.common.io.ByteStreams

import org.apache.spark.Logging
import org.apache.spark.serializer.Serializer
import org.apache.spark.util.Utils

import project.hawk.rddDepot._ 

/**
 * Stores BlockManager blocks on native memory.
 */
private[spark] class NativeStore(blockManager: BlockManager, diskManager: DiskBlockManager, defaultSerializer: Serializer)
  extends BlockStore(blockManager) with Logging {

  val capacity = blockManager.conf.getSizeAsBytes("spark.storage.nStoreCapacity", "1g").toLong
  val pageSize = blockManager.conf.getSizeAsBytes("spark.storage.nativeBufferSize", "1m").toInt

  val nStore = new NativeStorage(capacity, pageSize)

  override def getSize(blockId: BlockId): Long = {
    nStore.getSize(blockId.name)
  }

  override def putBytes(blockId: BlockId, _bytes: ByteBuffer, level: StorageLevel): PutResult = {
    logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in putBytes")
    val bytes = _bytes.duplicate()
    bytes.rewind()

    logDebug(s"Attempting to put block $blockId")

    var byteArray = Array.fill[Byte](bytes.remaining)(0)
    try {
      Utils.tryWithSafeFinally {
        bytes.get(byteArray);
      } {
       // nStore.put(blockId.name, byteArray)
      }
    } catch {
        case e: Throwable => throw e
    }
    PutResult(bytes.limit(), Right(bytes.duplicate()))
  }

  override def putArray(
      blockId: BlockId,
      values: Array[Any],
      level: StorageLevel,
      returnValues: Boolean): PutResult = {
    logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in putArray")
    putIterator(blockId, values.toIterator, level, returnValues)
  }


  // put/get style
  // override def putIterator(
  //     blockId: BlockId,
  //     values: Iterator[Any],
  //     level: StorageLevel,
  //     returnValues: Boolean): PutResult = {

  //   logDebug(s"Attempting to write values for block $blockId")
  //   logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in putIterator")
    
  //   val outputStream = new ByteArrayOutputStream(4096)
  //   try {
  //     Utils.tryWithSafeFinally {
  //       blockManager.dataSerializeStream(blockId, outputStream, values)
  //       nStore.put(blockId.name, outputStream.toByteArray)
  //     } {
  //       outputStream.close()
  //     }
  //   } catch {
  //       case e: Throwable => throw e
  //   }
  //   PutResult(nStore.getSize(blockId.name), null)
  // }


  // iterator style
  // override def putIterator(
  //     blockId: BlockId,
  //     values: Iterator[Any],
  //     level: StorageLevel,
  //     returnValues: Boolean): PutResult = {

  //   logDebug(s"Attempting to write values for block $blockId")
  //   logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in putIterator")
    
    
  //   val ser = defaultSerializer.newInstance()

  //   val nativeHandler = nStore.getHandler(blockId.name);

  //   try {
  //     Utils.tryWithSafeFinally {
  //       while(values.hasNext){
  //         // logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in hasNext")
  //         nativeHandler.add(ser.serialize(values.next).array)
  //       }
  //     } {
  //     }
  //   } catch {
  //       case e: Throwable => throw e
  //   }
  //   PutResult(nStore.getSize(blockId.name), null)
  // }


  // stream style
  override def putIterator(
      blockId: BlockId,
      values: Iterator[Any],
      level: StorageLevel,
      returnValues: Boolean): PutResult = {

    val spillFile = diskManager.getFile(blockId.name)
    val outputStream = nStore.getOutputStream(blockId.name, level.useSpill, spillFile);
    try {
      Utils.tryWithSafeFinally {
        blockManager.dataSerializeStreamDirect(blockId, outputStream, values)
      } {
        outputStream.close()
      }
    } catch {
        case e: Throwable => throw e
    }
    PutResult(nStore.getSize(blockId.name), null)
  }

  def putIterator2(
      blockId: BlockId,
      values: Iterator[Any],
      level: StorageLevel): Either[PutResult, Iterator[Any]] = {

    logDebug(s"Attempting to write values for block $blockId")
    logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in putIterator")
    
    val spillFile = diskManager.getFile(blockId.name)
    val outputStream = nStore.getOutputStream(blockId.name, level.useSpill, spillFile);
    if(outputStream == null) {
      Right(values)
    }
    else {
      try {
        Utils.tryWithSafeFinally {
          blockManager.dataSerializeStreamDirect(blockId, outputStream, values)
        } {
          outputStream.close()
        }
      } catch {
        case e: Throwable => 
          throw e
      }
      Left(PutResult(nStore.getSize(blockId.name), null))
    }
    
  }


  // put/get style... there is also put/get style  getValues method
  // override def getBytes(blockId: BlockId): Option[ByteBuffer] = {
  //   Some(ByteBuffer.wrap(nStore.get(blockId.name)))
  // }

   //iterator style
   // override def getBytes(blockId: BlockId): Option[ByteBuffer] = {
   //   val nativeHandler = nStore.getHandler(blockId.name)

   //   val buf = ByteBuffer.allocate(nStore.getSize(blockId.name).toInt)

   //   while(nativeHandler.hasNext) {
   //     buf.put(nativeHandler.next)
   //   }
   //   buf.flip();
   //   Some(buf)
   // }

  // stream style
  override def getBytes(blockId: BlockId): Option[ByteBuffer] = {
   val inputStream = nStore.getInputStream(blockId.name);
   val bs = new Array[Byte](nStore.getSize(blockId.name).toInt)
   ByteStreams.readFully(inputStream, bs)
   Some(ByteBuffer.wrap(bs))
  }

  // put/get style... there is also put/get style  getBytes method
  // override def getValues(blockId: BlockId): Option[Iterator[Any]] = {
  //   getBytes(blockId).map(buffer => blockManager.dataDeserialize(blockId, buffer))
  // }

  // iterator style
  // override def getValues(blockId: BlockId): Option[Iterator[Any]] = {
  //   logInfo("HAWK timestamp:" + System.nanoTime + " threadId:" + Thread.currentThread.getId + s" block $blockId in getValues")
  //   val nativeHandler = nStore.getHandler(blockId.name)
  //   nativeHandler.reset
  //   val nativeIter = new NativeStoreIterator(nativeHandler, defaultSerializer.newInstance())
  //   Some(nativeIter)
  // }

  // stream style
   override def getValues(blockId: BlockId): Option[Iterator[Any]] = {
     val inputStream = nStore.getInputStream(blockId.name);

     Option(inputStream).map { 
       inputStream => blockManager.dataDeserializeStreamDirect(blockId, inputStream)
     }
   }

  override def remove(blockId: BlockId): Boolean = {
    nStore.remove(blockId.name)
  }

  override def contains(blockId: BlockId): Boolean = {
    nStore.contains(blockId.name)
  }
}
