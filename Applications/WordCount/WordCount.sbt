name := "WordCount"

version := "1.0"

scalaVersion := "2.10.5"

resolvers += "Local Maven Repository" at "file:///home/ssahin7/.m2/repository"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1-SNAPSHOT"
