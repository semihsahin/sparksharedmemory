/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.rdd._
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.optimization.{SimpleUpdater, SquaredL2Updater, L1Updater}

import org.apache.log4j.{Level, Logger}

/**
 * An example app for linear regression. Run with
 * {{{
 * bin/run-example org.apache.spark.examples.mllib.LinearRegression
 * }}}
 * A synthetic dataset can be found at `data/mllib/sample_linear_regression_data.txt`.
 * If you use it as a template to create your own app, please use `spark-submit` to submit your app.
 */
object LinearRegression {

  object RegType extends Enumeration {
    type RegType = Value
    val NONE, L1, L2 = Value
  }

  import RegType._

  def main(args: Array[String]) {

    val inputFile = if (args.length > 0) args(0) else "data/mllib/sample_linear_regression_data.txt";
    val numIterations = if (args.length > 1) args(1).toInt else 10;
    val numFeatures = if (args.length > 2) args(2).toInt else 10;
    val stepSize = if (args.length > 3) args(3).toDouble else 1.0;
    val regType = L2;
    val regParam = if (args.length > 4) args(4).toDouble else 0.01;

    val conf = new SparkConf().setAppName(s"LinearRegression")
    val sc = new SparkContext(conf)

    Logger.getRootLogger.setLevel(Level.WARN)

    ///////////// Storage Level ////////////
    // val examples = sc.textFile(inputFile).map { line => 
    //   LabeledPoint.parse(line)
    // }

    // // val splits = examples.randomSplit(Array(0.8, 0.2))
    // // val training = splits(0).persist(NATIVE_ONLY)
    // // val test = splits(1).persist(NATIVE_ONLY)

    // // val numTraining = training.count()
    // // val numTest = test.count()
    // // println(s"Training: $numTraining, test: $numTest.")

    // // examples.unpersist()
    // examples.persist(NATIVE_ONLY)
    // val numExamples = examples.count()
    //////////////////////////////////////

    ///////////// Alluxio File ////////////
    val input = sc.textFile(inputFile).map { line => 
      LabeledPoint.parse(line)
    }

    val numExamples = input.count()
    input.saveAsObjectFile("alluxio://localhost:19998/LinearRegressionData") 
    val examples = sc.objectFile[LabeledPoint]("alluxio://localhost:19998/LinearRegressionData")
    //////////////////////////////////////

    val updater = regType match {
      case NONE => new SimpleUpdater()
      case L1 => new L1Updater()
      case L2 => new SquaredL2Updater()
    }

    val algorithm = new LinearRegressionWithSGD()
    algorithm.optimizer
      .setNumIterations(numIterations)
      .setStepSize(stepSize)
      .setUpdater(updater)
      .setRegParam(regParam)

    // val model = algorithm.run(training)
    val model = algorithm.run(examples)

    // val prediction = model.predict(test.map(_.features))
    // val predictionAndLabel = prediction.zip(test.map(_.label))c
    val prediction = model.predict(examples.map(_.features))
    val predictionAndLabel = prediction.zip(examples.map(_.label))

    val loss = predictionAndLabel.map { case (p, l) =>
      val err = p - l
      err * err
    }.reduce(_ + _)
    // val rmse = math.sqrt(loss / numTest)
    val rmse = math.sqrt(loss / numExamples)

    println(s"Test RMSE = $rmse.")

    sc.stop()
  }
}


// scalastyle:on println
